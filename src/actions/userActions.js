/*export function setName(name) {
    return dispatch => {
        setTimeout(() => {
            dispatch({
                type: 'SET_NAME',
                payload: name
            })
        }, 2000)
    }
}*/

// export function setName(name) {
//     return {
//         type: 'SET_NAME',
//         payload: new Promise((resolve,reject) => {
//             setTimeout(()=> {
//                 resolve(name);
//             },2000)
//         })
//     }
// }


// export function setName(name) {
//     return {
//         type: 'SET_NAME',
//         payload: axios.get('https://api.namsor.com/onomastics/api/json/gender/Andrea/Rossini')
//     }
// }

import axios from 'axios';

export function setName(name) {
    return dispatch => {
        axios.get('https://api.namsor.com/onomastics/api/json/gender/Andrea/Rossini')
            .then(bio => {
                dispatch({
                    type: 'SET_NAME',
                    payload: bio.data.firstName,
                })
            })
    }
}


export function setAge(age) {
    return {
        type: 'SET_AGE',
        payload: age
    }
}
