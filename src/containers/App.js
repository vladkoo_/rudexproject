import React from 'react';
import './App.css';
import  Main from '../components/main';
import User from '../components/user';
import {connect} from "react-redux";
import {setName} from '../actions/userActions';

function App(props) {
    return (
        <div className="App">
            <User username={props.user.name}/>
            <Main changeName={props.setName}/>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        math: state.match,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setName: (name) => {
            dispatch(setName(name))
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(App);
