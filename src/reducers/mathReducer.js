export default function mathReducer(state = {
    result: 1,
    lastValues: [],
}, action) {
    switch (action.type) {
        case 'ADD': {
            state = {
                ...state,
                result: state.result + action.payload,
                lastValues: [...state.lastValues, action.payload]
            };
            break;
        }
        case 'MINUS': {
            state = {
                ...state,
                result: state.result - action.payload,
                lastValues: [...state.lastValues, action.payload]
            };
            break;
        }
        default : {
            console.log('default');
            break;
        }
    }
    return state;
}
