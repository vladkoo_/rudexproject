export default function userReducer(state = {
    name: 'Vladik',
    age: 20,
}, action) {
    switch (action.type) {
        // case 'SET_NAME_FULFILLED': {
        case 'SET_NAME': {
            state = {
                ...state,
                // name: action.payload.data.firstName,
                name: action.payload,
            };
            break;
        }
        case 'SET_AGE': {
            state = {
                ...state,
                age: action.payload
            };
            break;
        }
        default : {
            console.log('default');
            break;
        }
    }
    return state;
}
